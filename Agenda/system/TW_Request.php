<?php

/**
* Classe responsável por obter os segmentos da URL informada
*
* @author Jose
* @access public
*/

class TW_Request {
    
    private $_controlador = "index";
    private $_metodo = "main";
    private $_args = array();
    
    /**
    * Método construtor
    * @access public
    * @return void
    */
   public function __construct()
   {
       //algum controlador foi informado na URL? se nao foi, mantem que o controlador e o 'index'.
       if(!isset($_GET["url"])) return FALSE;

       //explde os segmentos da URL e os armazena em um array
       $segmentos = explode('/', $_GET["url"]);
       
       //se o controlador foi realmente definido, retorna o nome dele.
       $this->_controlador = ($c = array_shift($segmentos)) ? $c : 'index';
       
       //se um metodo foi realmente requisitado, retorna o nome dele.
       $this->_metodo = ($m = array_shift($segmentos)) ? $m : 'main';
       
       //se argumentos adicionais foram definidos, os retorna um array.
       $this->_args = (isset($segmentos[0])) ? $segmentos : array();
   }
   
   /**
    * retorna nome do controlador
    * @access public
    * @return void
    */
   public function getControlador()
   {
       return$this->_controlador;
   }
   
   /**
    * retorna o nome do metodo
    * @access public
    * @return void
    */
   public function getMetodo()
   {
       return $this->_metodo;
   }
   
   /**
    * retorna os segmentos adicionais (argumentos)
    * @access public
    * @return void
    */
   public function getArgs()
   {
       return $this->_args;
   }
}